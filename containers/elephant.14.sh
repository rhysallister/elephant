#!/bin/bash
set -x

POSTGIS_MAJOR=3
PG_MAJOR=14
PGSQL_HTTP_VERSION=master

pgc=$(buildah from --name elephant:14 docker.io/library/postgres:$PG_MAJOR)

xtraDeps="git ca-certificates build-essential dh-autoreconf libcurl4-openssl-dev \
  libssl-dev curl libsodium-dev cmake libkrb5-dev systemtap-sdt-dev"

buildah run $pgc apt-get update 
buildah run $pgc apt-get install -y --no-install-recommends $xtraDeps

buildah run $pgc  curl -o deb.sh https://install.citusdata.com/community/deb.sh
buildah run $pgc bash deb.sh
buildah run $pgc rm deb.sh
buildah run $pgc apt-get install -y --no-install-recommends \
  postgresql-server-dev-$PG_MAJOR \
  postgresql-$PG_MAJOR-postgis-$POSTGIS_MAJOR \
  postgresql-$PG_MAJOR-postgis-$POSTGIS_MAJOR-scripts \
  postgresql-$PG_MAJOR-plsh \
  postgresql-$PG_MAJOR-partman \
  postgresql-$PG_MAJOR-pgrouting \
  postgresql-$PG_MAJOR-ogr-fdw \
  postgresql-$PG_MAJOR-pgtap \
  postgresql-$PG_MAJOR-pldebugger \
  postgresql-$PG_MAJOR-cron \
  postgresql-$PG_MAJOR-pgmp \
  postgresql-$PG_MAJOR-pointcloud \
  postgresql-$PG_MAJOR-citus-10.2 \
  postgresql-$PG_MAJOR-plr \
  postgresql-$PG_MAJOR-pllua \
  postgresql-$PG_MAJOR-hll \
  postgresql-$PG_MAJOR-pglogical \
  postgresql-$PG_MAJOR-pglogical-ticker \
  postgresql-$PG_MAJOR-pgl-ddl-deploy \
  postgresql-$PG_MAJOR-numeral \
  postgresql-plpython3-$PG_MAJOR \
  python3-fake-factory \
  python3-boto3 \
  libsodium23 \
  zlib1g-dev \
  parallel \
  python3-requests \
  gdal-bin \
  make

buildah run $pgc apt-get install pgxnclient


buildah run $pgc pgxn install h3
buildah run $pgc pgxn install http
buildah run $pgc pgxn install ddlx
buildah run $pgc pgxn install safeupdate 
buildah run $pgc pgxn install pg_task
buildah run $pgc pgxn install postgresql_anonymizer
#buildah run $pgc pgxn install osm_fdw
buildah run $pgc pgxn install pgbitmap
buildah run $pgc pgxn install gzip
#buildah run $pgc pgxn install plv8

  # Install pgjwt
buildah config --workingdir /tmp/ephemeral $pgc
buildah run $pgc git clone https://github.com/michelp/pgjwt.git
buildah config --workingdir /tmp/ephemeral/pgjwt $pgc
buildah run $pgc git checkout master
buildah run $pgc make install

# install pgsql-addressing-dictionary
 buildah config --workingdir /tmp/ephemeral $pgc
 buildah run $pgc git clone https://github.com/pramsey/pgsql-addressing-dictionary.git
 buildah config --workingdir /tmp/ephemeral/pgsql-addressing-dictionary $pgc
 buildah run $pgc git checkout master
 buildah run $pgc make
 buildah run $pgc make install

  # Install pgsodium
buildah config --workingdir /tmp/ephemeral $pgc
buildah run $pgc git clone https://github.com/michelp/pgsodium.git
buildah config --workingdir /tmp/ephemeral/pgsodium $pgc
buildah run $pgc git checkout 1.2.0
buildah run $pgc make 
buildah run $pgc make install

  # Install pghash_ids
buildah config --workingdir /tmp/ephemeral $pgc
buildah run $pgc git clone https://github.com/iCyberon/pg_hashids.git
buildah config --workingdir /tmp/ephemeral/pg_hashids $pgc
buildah run $pgc git checkout master
buildah run $pgc make 
buildah run $pgc make install

  # Install pgfaker
buildah config --workingdir /tmp/ephemeral $pgc
buildah run $pgc git clone https://gitlab.com/dalibo/postgresql_faker.git 
buildah config --workingdir /tmp/ephemeral/postgresql_faker $pgc
buildah run $pgc git checkout 0.1.0
buildah run $pgc make install


  # clean up
buildah config --workingdir / $pgc
buildah run $pgc rm -rf /tmp/ephemeral/
#buildah run $pgc apt-get remove -y $xtraDeps
buildah run $pgc apt-get purge -y
buildah run $pgc apt-get autoremove -y
buildah run $pgc apt-get clean
buildah run $pgc rm -rf /var/lib/apt/lists/* /var/tmp/* /tmp/*


  # final thoughts
buildah config --workingdir /root $pgc
buildah run $pgc touch /root/🐘


  # commit
buildah commit --squash $pgc registry.gitlab.com/rhysallister/elephant:14

