#!/bin/bash
set -x

POSTGIS_MAJOR=3
PG_MAJOR=17
PGSQL_HTTP_VERSION=master

WHATVER=17-3.5

pgc=$(buildah from --name elephant:$PG_MAJOR docker.io/postgis/postgis:$WHATVER)

xtraDeps="git ca-certificates build-essential dh-autoreconf libcurl4-openssl-dev \
  libssl-dev curl libsodium-dev libkrb5-dev systemtap-sdt-dev"

buildah run $pgc apt-get update
buildah run $pgc apt-get install -y --no-install-recommends $xtraDeps
# buildah run $pgc curl -o cmake.sh \
# 	https://github.com/Kitware/CMake/releases/download/v3.25.3/cmake-3.25.3-linux-x86_64.sh
#
# buildah run $pgc bash cmake.sh

#buildah run $pgc curl -o deb.sh https://install.citusdata.com/community/deb.sh
#buildah run $pgc bash deb.sh
#buildah run $pgc rm deb.sh

buildah run $pgc apt-get install -y postgresql-$PG_MAJOR
buildah run $pgc apt-get install -y postgresql-server-dev-$PG_MAJOR
buildah run $pgc apt-get install -y postgresql-client-$PG_MAJOR

buildah run $pgc apt-get install -y \
	postgresql-$PG_MAJOR-cron \
	postgresql-$PG_MAJOR-extra-window-functions \
	postgresql-$PG_MAJOR-hll \
	postgresql-$PG_MAJOR-numeral \
	postgresql-$PG_MAJOR-ogr-fdw \
	postgresql-$PG_MAJOR-partman \
	postgresql-$PG_MAJOR-pgl-ddl-deploy \
	postgresql-$PG_MAJOR-pglogical \
	postgresql-$PG_MAJOR-pglogical-ticker \
	postgresql-$PG_MAJOR-pgmp \
	postgresql-$PG_MAJOR-pgmp \
	postgresql-$PG_MAJOR-pgsphere \
	postgresql-$PG_MAJOR-pgtap \
	postgresql-$PG_MAJOR-pgvector \
	postgresql-$PG_MAJOR-pldebugger \
	postgresql-$PG_MAJOR-pllua \
	postgresql-$PG_MAJOR-plpgsql-check \
	postgresql-$PG_MAJOR-plprofiler \
	postgresql-$PG_MAJOR-plr \
	postgresql-$PG_MAJOR-plsh \
	postgresql-$PG_MAJOR-q3c \
	postgresql-$PG_MAJOR-rational \
	postgresql-$PG_MAJOR-repack \
	postgresql-$PG_MAJOR-rum \
	postgresql-$PG_MAJOR-set-user \
	postgresql-$PG_MAJOR-show-plans \
	postgresql-$PG_MAJOR-similarity \
	postgresql-$PG_MAJOR-tablelog \
	postgresql-$PG_MAJOR-tds-fdw \
	postgresql-$PG_MAJOR-toastinfo \
	postgresql-$PG_MAJOR-unit \
	postgresql-$PG_MAJOR-wal2json \
	postgresql-plpython3-$PG_MAJOR \
	python3-fake-factory \
	libsodium23 \
	parallel \
	python3-requests \
	gdal-bin \
	make

buildah run $pgc apt-get install -y \
	postgis \
	postgresql-postgis \
	postgresql-$PG_MAJOR-postgis-$POSTGIS_MAJOR \
	postgresql-$PG_MAJOR-postgis-$POSTGIS_MAJOR-scripts \
	postgresql-$PG_MAJOR-pgrouting \
	postgresql-$PG_MAJOR-pointcloud

# buildah config --workingdir /tmp/ephemeral $pgc
# buildah run $pgc curl -O \
# 	https://gitlab.kitware.com/cmake/cmake/-/archive/v3.25.3/cmake-v3.25.3.tar.gz
# buildah run $pgc tar -xzf cmake-v3.25.3.tar.gz
# buildah config --workingdir /tmp/ephemeral/cmake-v3.25.3 $pgc
# buildah run $pgc ./bootstrap
# buildah run $pgc gmake
# buildah run $pgc make install

buildah run $pgc apt-get install -y pgxnclient

buildah run $pgc pgxn install h3
buildah run $pgc pgxn install http
buildah run $pgc pgxn install ddlx
buildah run $pgc pgxn install safeupdate
#buildah run $pgc pgxn install pg_task
buildah run $pgc pgxn install postgresql_anonymizer
#buildah run $pgc pgxn install osm_fdw
buildah run $pgc pgxn install pg_extra_time
buildah run $pgc pgxn install pgbitmap
buildah run $pgc pgxn install gzip
#buildah run $pgc pgxn install plv8

# Install pgjwt
buildah config --workingdir /tmp/ephemeral $pgc
buildah run $pgc git clone https://github.com/michelp/pgjwt.git
buildah config --workingdir /tmp/ephemeral/pgjwt $pgc
buildah run $pgc git checkout master
buildah run $pgc make install

# install pgsql-addressing-dictionary
buildah config --workingdir /tmp/ephemeral $pgc
buildah run $pgc git clone https://github.com/pramsey/pgsql-addressing-dictionary.git
buildah config --workingdir /tmp/ephemeral/pgsql-addressing-dictionary $pgc
buildah run $pgc git checkout master
buildah run $pgc make
buildah run $pgc make install

# Install pgsodium
buildah config --workingdir /tmp/ephemeral $pgc
buildah run $pgc git clone https://github.com/michelp/pgsodium.git
buildah config --workingdir /tmp/ephemeral/pgsodium $pgc
buildah run $pgc git checkout 1.2.0
buildah run $pgc make
buildah run $pgc make install

# Install pghash_ids
buildah config --workingdir /tmp/ephemeral $pgc
buildah run $pgc git clone https://github.com/iCyberon/pg_hashids.git
buildah config --workingdir /tmp/ephemeral/pg_hashids $pgc
buildah run $pgc git checkout master
buildah run $pgc make
buildah run $pgc make install

# Install pgfaker
buildah config --workingdir /tmp/ephemeral $pgc
buildah run $pgc git clone https://gitlab.com/dalibo/postgresql_faker.git
buildah config --workingdir /tmp/ephemeral/postgresql_faker $pgc
buildah run $pgc git checkout 0.1.0
buildah run $pgc make install

# clean up
buildah config --workingdir / $pgc
buildah run $pgc rm -rf /tmp/ephemeral/
#buildah run $pgc apt-get remove -y $xtraDeps
buildah run $pgc apt-get purge -y
buildah run $pgc apt-get autoremove -y
buildah run $pgc apt-get clean
buildah run $pgc rm -rf /var/lib/apt/lists/* /var/tmp/* /tmp/*

# final thoughts
buildah config --workingdir /root $pgc
buildah run $pgc touch /root/🐘

# commit
#buildah commit --squash $pgc registry.gitlab.com/rhysallister/elephant:$PG_MAJOR
